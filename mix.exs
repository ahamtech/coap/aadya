defmodule Aadya.Mixfile do
  use Mix.Project

  def project do
    [
      app: :aadya,
      version: "0.1.0",
      elixir: "~> 1.5",
      compilers: [:phoenix] ++ Mix.compilers(),
      elixirc_paths: elixirc_paths(Mix.env()),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      applications: [
        :logger,
        :poolboy,
        :phoenix,
        :phoenix_html,
        :cowboy,
        :wobserver
      ]
    ]
  end

  defp elixirc_paths(_), do: ["lib", "web"]

  defp deps do
    [
      {:typex, "~> 0.1.0"},
      {:poolboy, "~> 1.5.1"},
      {:credo, "~> 0.8.10", only: [:dev, :test]},
      {:phoenix, "~> 1.3.0"},
      {:phoenix_html, "~> 2.10"},
      {:ex_doc, "~> 0.18.1", only: :dev, runtime: false},
      {:poison, "~> 3.1"},
      {:cowboy, "~> 1.1"},
      {:socket, "~> 0.3.12", only: [:test]},
      {:dialyxir, "~> 0.5", only: [:dev], runtime: false},
      {:wobserver, "~> 0.1.8"},
      {:benchee, "~> 0.12.0"}
    ]
  end
end
