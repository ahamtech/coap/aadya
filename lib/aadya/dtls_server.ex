defmodule Aadya.DtlsServer do
  @moduledoc ~S"""
  DTLS Coap Server
  """
  use GenServer
  require Logger
  require Record
  @timeout 60_000
  Record.defrecord(:state, sock: nil, chans: nil, pool: nil)

  def start_link(serverName, inPort) do
    GenServer.start_link(__MODULE__, [inPort], [])
  end

  def init([host, inPort]) do
    Logger.info("Coap Server Started at port #{inPort}")

    {:ok, socket} =
      :ssl.connect(host, inPort, [
        :binary,
        {:protocol, :dtls},
        {:active, true},
        {:reuseaddr, true}
      ])
  end

  def handle_info(
        {:ssl, {peerIP, peerPortNo}, data},
        state(sock: socket) = state
      ) do
    :ok = :ssl.send(socket, peerIP, peerPortNo, data)
    {:noreply, state}
  end

  # def handle_cast(:accept, state(sock: listensocket)=state) do
  #       {:ok, socket} = :ssl.transport_accept(listensocket)
  #       # % create a new acceptor to maintain a set of waiting acceptors
  #     CoapDtlsListen.start_socket()
  #     # % establish the connection
  #     :ok = :ssl.ssl_accept(socket)
  #     # % FIXME: where do we get the chanel id?
  #     {:ok, supPid, pid} = CoapChannelSup.start_link(self(), {{0,0,0,0}, 0})
  #     {:noreply, state(state,[sock: socket, supid: supPid, channel: pid])}
  # end

  def handle_cast(:shutdown, state) do
    {:stop, :normal, state}
  end

  def handle_info({:ssl_closed, _socket}, state) do
    {:stop, :normal, state}
  end

  def handle_info({:datagram, _chid, data}, state(sock: socket)) do
    :ok = :ssl.send(socket, data)
    {:noreply, state}
  end

  def handle_info({:terminated}, state(sock: socket) = state) do
    :ssl.close(socket)
    {:stop, :normal, state}
  end

  def handle_info(info, state) do
    Logger.warn("coap_dtls_socket unexpected ~p~n", [info])
    {:noreply, state}
  end

  def code_change(_OldVsn, state, _Extra) do
    {:ok, state}
  end
end
