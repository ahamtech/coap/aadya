defmodule Aadya.Router do
  @moduledoc ~S"""
  Coap Router Module
  """

  @doc """
  Resposne to Router
  ```
    defmodule Hello do
      def hello path do
        case :ets.lookup :resource,path do
          [] ->
            {:not_found, :not_found}
          [{_,data}] ->
            { :created , data }
          end
      end
      def post path,data do
        case :ets.insert(:resource,{path,data}) do
          true ->
            {:ok, data}
          _ ->
            IO.puts "something went worong"
            {:error, error}
        end
      end
      def put path,data do
        case :ets.insert(:resource,{path,data}) do
          true ->
            {:ok, data}
          _ ->
            IO.puts "something went worong"
            {:error, "erroror"}
        end
      end
    end
    ```

    Resposne Codes:
    ```
    (:content) ->: {2, 0}
    (:created) ->: {2, 1}
    (:deleted) ->: {2, 2}
    (:valid) ->: {2, 3}
    (:changed) ->: {2, 4}
    (:continue) ->: {2,31}

    (:bad_request) ->: {4, 0}
    (:unauthorized) ->: {4, 1}
    (:bad_option) ->: {4, 2}
    (:not_found) ->: {4, 4}
    (:method_not_allowed) ->: {4, 5}
    (:not_acceptable) ->: {4, 6}
    (:request_entity_incomplete) ->: {4, 7}
    (:request_entity_incomplete) ->: {4, 8}
    (:precondition_failed) ->: {4, 12}
    (:request_entity_too_large) ->: {4, 13}
    (:unsupported_content_format) ->: {4, 15}

    (:internal_server_error) ->: {5, 0}
    (:not_implemented) ->: {5, 1}
    (:bad_gateway) ->: {5, 2}
    (:service_unavailable) ->: {5, 3}
    (:gateway_timeout) ->: {5, 4}
    (:proxying_not_supported) ->: {5, 5}
    ```

  """
  defmacro __using__(_options) do
    quote do
      import unquote(__MODULE__)
      Module.register_attribute(__MODULE__, :get_resources, accumulate: true)
      Module.register_attribute(__MODULE__, :put_resources, accumulate: true)
      Module.register_attribute(__MODULE__, :post_resources, accumulate: true)
      Module.register_attribute(__MODULE__, :delete_resources, accumulate: true)

      Module.register_attribute(
        __MODULE__,
        :unobserve_resources,
        accumulate: true
      )

      @before_compile unquote(__MODULE__)
    end
  end

  defmacro __before_compile__(_options) do
    quote do
      def resource_get(path) do
        {:not_found, path}
      end

      def resource_put(path, payload) do
        {:not_found, path}
      end

      def resource_delete(path) do
        {:not_found, path}
      end

      def resource_post(path, payload) do
        {:not_found, path}
      end

      def send_message(observers_list, data) do
        observers_list
        |> Enum.map(fn observer_pid -> send(observer_pid, {:send, data}) end)
      end

      def get_resources, do: @get_resources
      def post_resources, do: @post_resources
      def put_resources, do: @put_resources
      def delete_resources, do: @delete_resources
      def unobserve_resources, do: @unobserve_resources
    end
  end

  defmacro get(path, module, function) do
    quote do
      @get_resources unquote(path)
      def resource_get(unquote(path)) do
        apply(unquote(module), unquote(function), [unquote(path)])
      end
    end
  end

  defmacro put(path, module, function) do
    quote do
      @put_resources unquote(path)
      def resource_put(unquote(path), payload) do
        apply(unquote(module), unquote(function), [unquote(path), payload])
      end
    end
  end

  defmacro delete(path, module, function) do
    quote do
      @delete_resources unquote(path)
      def resource_delete(unquote(path), payload) do
        apply(unquote(module), unquote(function), [unquote(path), payload])
      end
    end
  end

  defmacro post(path, module, function) do
    quote do
      @post_resources unquote(path)
      def resource_post(unquote(path), payload) do
        apply(unquote(module), unquote(function), [unquote(path), payload])
      end
    end
  end

  defmacro unobserve(path) do
    quote do
      @unobserve_resources unquote(path)
    end
  end
end
