defmodule Aadya.Header do
  @moduledoc ~S"""
  Coap Headers
  """
  require Record

  defmacro __using__(_) do
    quote do
      @max_block_size 1024
      @default_max_age 60

      @typedoc """
      just coap_message record type
      """
      @type coap_message :: coap_message()

      @typedoc """
      just coap_message record type
      """
      @type coap_content :: coap_content()
      Record.defrecord(
        :coap_message,
        type: nil,
        method: nil,
        id: nil,
        token: <<>>,
        options: [],
        payload: <<>>
      )

      Record.defrecord(
        :coap_content,
        etag: nil,
        max_age: @default_max_age,
        format: nil,
        payload: <<>>
      )
    end
  end

  def content_formats(0), do: <<"text/plain">>
  def content_formats(40), do: <<"application/link-format">>
  def content_formats(41), do: <<"application/xml">>
  def content_formats(42), do: <<"application/octet-stream">>
  def content_formats(47), do: <<"application/exi">>
  def content_formats(50), do: <<"application/json">>
  def content_formats(60), do: <<"application/cbor">>

  def decode_type(0), do: :con
  def decode_type(1), do: :non
  def decode_type(2), do: :ack
  def decode_type(3), do: :reset

  def encode_type(:con), do: 0
  def encode_type(:non), do: 1
  def encode_type(:ack), do: 2
  def encode_type(:reset), do: 3

  def methods({0, 1}), do: :get
  def methods({0, 2}), do: :post
  def methods({0, 3}), do: :put
  def methods({0, 4}), do: :delete

  def methods({2, 0}), do: :content
  def methods({2, 1}), do: :created
  def methods({2, 2}), do: :deleted
  def methods({2, 3}), do: :valid
  def methods({4, 4}), do: :not_found

  def methods(:get), do: {0, 1}
  def methods(:post), do: {0, 2}
  def methods(:put), do: {0, 3}
  def methods(:delete), do: {0, 4}

  def methods(:content), do: {2, 0}
  def methods(:created), do: {2, 1}
  def methods(:deleted), do: {2, 2}
  def methods(:valid), do: {2, 3}
  def methods(:changed), do: {2, 4}
  def methods(:continue), do: {2, 31}

  def methods(:bad_request), do: {4, 0}
  def methods(:unauthorized), do: {4, 1}
  def methods(:bad_option), do: {4, 2}
  def methods(:not_found), do: {4, 4}
  def methods(:method_not_allowed), do: {4, 5}
  def methods(:not_acceptable), do: {4, 6}
  def methods(:request_entity_incomplete), do: {4, 7}
  def methods(:request_entity_incomplete), do: {4, 8}
  def methods(:precondition_failed), do: {4, 12}
  def methods(:request_entity_too_large), do: {4, 13}
  def methods(:unsupported_content_format), do: {4, 15}

  def methods(:internal_server_error), do: {5, 0}
  def methods(:not_implemented), do: {5, 1}
  def methods(:bad_gateway), do: {5, 2}
  def methods(:service_unavailable), do: {5, 3}
  def methods(:gateway_timeout), do: {5, 4}
  def methods(:proxying_not_supported), do: {5, 5}
end
