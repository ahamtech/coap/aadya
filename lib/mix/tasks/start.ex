defmodule Mix.Tasks.Aadya.Start do
  use Mix.Task
  @shortdoc "starting the phoenix server "

  def run(_options) do
    apply(app_name, :start, [])
  end

  defp app_name() do
   appname =
     Mix.Project.config()[:app]
     |> to_string
     |> String.capitalize()
   Module.concat(Elixir, appname)
  end
end
