# Getting Started

[Aadya](http://aadya.ahamtech.in)


[Getting Started](http://aadya.ahamtech.in/start.html)

## Demo

For demo project please check this project [Demo](https://gitlab.com/ahamtech/coap/demo)

## Logging

[Gralog](http://aadya.ahamtech.in/2018/01/25/Logging-Aadya-on-Graylog.html)

[Papertrail](http://aadya.ahamtech.in/2017/04/12/Logging-Aadya-using-Papertrail.html)

[CloudWatch](http://aadya.ahamtech.in/2018/02/22/Logging-Cloud-Watch.html)

## Benchmarks

```elixir
mix run sample/run.exs
```

#### MessageParser

![image](/priv/static/images/image.png)

## License
Aadya is Copyright © Ahamtech. It is free software, and may be redistributed under the terms specified in the LICENSE file.

## About Ahamtech

![img](https://sos-ch-dk-2.exo.io/aham-web/company/ahamtech.png)

> Aadya is maintained by Ahamtech.

We love open source software, Erlang, Elixir, and Phoenix. See our other opensource projects, or hire our Elixir Phoenix development team to design, develop, and grow your product.
