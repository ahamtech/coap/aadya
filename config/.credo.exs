# config/.credo.exs
%{
  configs: [
    %{
      name: "default",
      files: %{
        included: ["lib/", "src/", "web/", "apps/"],
        excluded: []
      },
      checks: [
        {Credo.Check.Consistency.TabsOrSpaces},
        {Credo.Check.Design.AliasUsage, priority: :low},
        {Credo.Check.Readability.MaxLineLength, priority: :low, max_length: 180},
        {Credo.Check.Readability.Specs, priority: :low, max_length: 180},
        {Credo.Check.Readability.ModuleDoc, priority: :low},
#        {Credo.Check.Readability.SnakeCase},
        {Credo.Check.Design.TagTODO, exit_status: 2},
        {Credo.Check.Design.TagFIXME, false}
      ]
    }
  ]
}
