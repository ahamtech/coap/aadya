use Mix.Config

# Configures the endpoint
config :aadya, web: true

config :aadya, Aadya.Endpoint,
  url: [host: "localhost"],
  secret_key_base:
    "6wUbtNztAqYI/Bn1o2UZ5+BqC4q4PzptFiO3jYMrFOc47wY21hCk4gfdhuqG6Rwd",
  render_errors: [view: Aadya.ErrorView, accepts: ~w(html json)],
  http: [port: 4000],
  debug_errors: true

config :wobserver,
  mode: :plug,
  remote_url_prefix: "/wobserver"

config :logger, backends: [:console]

import_config "#{Mix.env()}.exs"
