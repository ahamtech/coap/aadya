defmodule AadyaServer do
  use ExUnit.Case

  defmodule Hello do
    def get(path) do
      {:content, path}
    end

    def post(path, data) do
      {:created, {path}}
    end

    def put(path, data) do
      {:created, {path}}
    end

    def del(path, data) do
      {:deleted, {path, data}}
    end
  end

  defmodule Test do
    use Aadya.Router
    get("/", Hello, :get)
    put("/", Hello, :put)
    post("/", Hello, :post)
    delete("/", Hello, :del)

    get("/13", Hello, :get)
    post("/13", Hello, :post)
    put("/13", Hello, :put)
    delete("/13", Hello, :del)
  end

  setup_all do
    Aadya.Server.start_link(Test)
    :ok
  end

  test "init" do
    host = {127, 0, 0, 1}
    coap_port = 5608
    server_port = 5683
    msg_ping = <<54, 89, 23, 56>>

    client = Socket.UDP.open!(coap_port)

    Socket.Datagram.send!(client, msg_ping, {host, server_port})
    {response, _} = Socket.Datagram.recv!(client)

    assert msg_ping == response
  end

  test "get" do
    host = {127, 0, 0, 1}
    coap_port = 5608
    server_port = 5683
    client = Socket.UDP.open!(coap_port)

    msg_ping =
      Aadya.MessageParser.encode(
        {:coap_message, :con, :get, 4020, "", [uri_path: ["13"]], ""}
      )

    Socket.Datagram.send!(client, msg_ping, {host, server_port})
    {response, _} = Socket.Datagram.recv!(client)

    assert {true,
            {:coap_message, :ack, :content, 4020, "", [uri_path: ["13"]], "/13"}} ==
             Aadya.MessageParser.decode(response)
  end

  test "post" do
    host = {127, 0, 0, 1}
    coap_port = 5608
    server_port = 5683
    client = Socket.UDP.open!(coap_port)

    msg_ping =
      Aadya.MessageParser.encode(
        {:coap_message, :con, :post, 2206, "", [uri_path: ["13"]], "12"}
      )

    Socket.Datagram.send!(client, msg_ping, {host, server_port})
    {response, _} = Socket.Datagram.recv!(client)

    assert {true, {:coap_message, :ack, :created, 2206, "", [], ""}} ==
             Aadya.MessageParser.decode(response)
  end

  test "put" do
    host = {127, 0, 0, 1}
    coap_port = 5608
    server_port = 5683
    client = Socket.UDP.open!(coap_port)

    msg_ping =
      Aadya.MessageParser.encode(
        {:coap_message, :con, :put, 3491, "", [uri_path: ["13"]], "123456"}
      )

    Socket.Datagram.send!(client, msg_ping, {host, server_port})
    {response, _} = Socket.Datagram.recv!(client)

    assert {true, {:coap_message, :ack, :created, 3491, "", [], ""}} ==
             Aadya.MessageParser.decode(response)
  end

  test "delete" do
    host = {127, 0, 0, 1}
    coap_port = 5608
    server_port = 5683
    client = Socket.UDP.open!(coap_port)

    msg_ping =
      Aadya.MessageParser.encode(
        {:coap_message, :con, :delete, 3423, "", [uri_path: ["13"]], ""}
      )

    Socket.Datagram.send!(client, msg_ping, {host, server_port})
    {response, _} = Socket.Datagram.recv!(client)

    assert {true,
            {:coap_message, :ack, :deleted, 3423, "", [uri_path: ["13"]], ""}} ==
             Aadya.MessageParser.decode(response)
  end

  test "not found" do
    host = {127, 0, 0, 1}
    coap_port = 5608
    server_port = 5683
    client = Socket.UDP.open!(coap_port)

    msg_ping =
      Aadya.MessageParser.encode(
        {:coap_message, :con, :get, 846, "", [uri_path: ["12"]], ""}
      )

    Socket.Datagram.send!(client, msg_ping, {host, server_port})
    {response, _} = Socket.Datagram.recv!(client)

    assert {true,
            {:coap_message, :ack, :not_found, 846, "", [uri_path: ["12"]],
             "/12"}} == Aadya.MessageParser.decode(response)
  end

  # test "Buffer Put" do
  #   host = {127, 0, 0, 1}
  #   coap_port = 5608
  #   server_port = 5683
  #   client = Socket.UDP.open!(coap_port)
  #
  #   msg_ping = Aadya.MessageParser.encode {:coap_message, :con, :put, 23757, "", [uri_path: ["/"]], "They hajsksksmsnsnsnsns"}
  #
  #   Socket.Datagram.send!(client, msg_ping,{host, server_port})
  #   {response, _ } = Socket.Datagram.recv!(client)
  #
  #   assert {true, {:coap_message, :ack, :created, 23757, "", [], "/"}} == Aadya.MessageParser.decode response
  # end
end
