defmodule AadyaMessageTest do
  use ExUnit.Case

  test "Coap Message Decoding -- error " do
    assert {_, <<4, 0, 15>>} = Aadya.MessageParser.decode(<<4, 0, 15>>)
  end

  test "Coap Message Decoding -- ping" do
    {true, message} = Aadya.MessageParser.decode(<<64, 0, 15, 138>>)
    {:coap_message, type, method, msgId, "", [], ""} = message
    assert type == :con
    assert method == nil
    assert msgId == 3978
  end

  test "Coap Message Encoding -- ping" do
    assert <<64, 0, 15, 138>> ==
             Aadya.MessageParser.encode(
               {:coap_message, :con, nil, 3978, "", [], ""}
             )
  end

  test "Coap Message Decoding -- get" do
    {true, message} = Aadya.MessageParser.decode(<<64, 1, 92, 205>>)
    {:coap_message, type, method, msgId, "", [], ""} = message
    assert type == :con
    assert method == :get
    assert msgId == 23757
  end

  test "Coap Message Encoding -- get" do
    assert <<64, 1, 92, 205>> ==
             Aadya.MessageParser.encode(
               {:coap_message, :con, :get, 23757, "", [], ""}
             )
  end

  test "Coap Message Decoding -- post" do
    {true, message} = Aadya.MessageParser.decode(<<64, 2, 92, 205>>)
    {:coap_message, type, method, msgId, "", [], ""} = message
    assert type == :con
    assert method == :post
    assert msgId == 23757
  end

  test "Coap Message Encoding -- post" do
    assert <<64, 2, 92, 205>> ==
             Aadya.MessageParser.encode(
               {:coap_message, :con, :post, 23757, "", [], ""}
             )
  end

  test "Coap Message Decoding -- put" do
    {true, message} = Aadya.MessageParser.decode(<<64, 3, 92, 205>>)
    {:coap_message, type, method, msgId, "", [], ""} = message
    assert type == :con
    assert method == :put
    assert msgId == 23757
  end

  test "Coap Message Encoding -- put" do
    assert <<64, 3, 92, 205>> ==
             Aadya.MessageParser.encode(
               {:coap_message, :con, :put, 23757, "", [], ""}
             )
  end

  test "Coap Message Decoding -- delete" do
    {true, message} = Aadya.MessageParser.decode(<<64, 4, 92, 205>>)
    {:coap_message, type, method, msgId, "", [], ""} = message
    assert type == :con
    assert method == :delete
    assert msgId == 23757
  end

  test "Coap Message Encoding -- delete" do
    assert <<64, 4, 92, 205>> ==
             Aadya.MessageParser.encode(
               {:coap_message, :con, :delete, 23757, "", [], ""}
             )
  end

  test "Coap Message Decoding -- payload" do
    {true, message} = Aadya.MessageParser.decode(<<64, 1, 92, 205, 209, 10, 2>>)
    {:coap_message, type, method, msgId, "", block, ""} = message
    assert type == :con
    assert method == :get
    assert msgId == 23757
    assert [block2: {0, false, 64}] == block
  end

  test "Coap Message Encoding -- payload" do
    assert <<64, 1, 92, 205, 209, 10, 2>> ==
             Aadya.MessageParser.encode(
               {:coap_message, :con, :get, 23757, "", [block2: {0, false, 64}],
                ""}
             )
  end
end
