defmodule AadyaServerRouter do
  use ExUnit.Case

  defmodule Hello do
    def get(path) do
      {:content, path}
    end

    def put(path, data) do
      {:created, {path, data}}
    end

    def post(path, data) do
      {:created, {path, data}}
    end

    def del(path, data) do
      {:deleted, {path, data}}
    end
  end

  defmodule Test do
    use Aadya.Router
    get("/", Hello, :get)
    put("/", Hello, :put)
    post("/", Hello, :post)
    delete("/", Hello, :del)
    get("/123", Hello, :get)
    put("/23", Hello, :put)
    post("/13", Hello, :post)
    delete("/31", Hello, :del)
  end

  test "Router Get" do
    assert ["/123", "/"] = Test.get_resources()
  end

  test "Router Put" do
    assert ["/23", "/"] = Test.put_resources()
  end

  test "Router post" do
    assert ["/13", "/"] = Test.post_resources()
  end

  test "Router delete" do
    assert ["/31", "/"] = Test.delete_resources()
  end
end
