Benchee.run(%{
  "coap_message_get" => fn -> Aadya.MessageParser.encode {:coap_message, :con, :get, 23757, "", [], ""} end,
  "coap_message_post" => fn -> Aadya.MessageParser.encode {:coap_message, :con, :post, 23757, "", [], ""} end,
  "coap_message_parser_get" => fn -> Aadya.MessageParser.decode <<64, 0, 15, 138>> end,
  "coap_message_parser_post" => fn -> Aadya.MessageParser.decode <<64, 2, 92, 205>> end

})
